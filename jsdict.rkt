#lang racket

(require syntax-parse-example/js-dict/js-dict)

(define tbl (hash 'a 1 'b 2 'c 3))

(js-extract {a b #:rest tbl2} tbl)

a b tbl2
