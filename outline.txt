
pros:


cons:
- regret no email "ping" for each entry ... was great aspect of pictures


### OUTLINE 2021-10-25

- Title: Fun and Games 2 // Ben Greenman // PLT at Brown University
- Lucky to use Racket every day (picture of year-round? sunrise/set?)
- but. I must confess.
- Often a grind
  - 
  - installing proper custodian
  - ... other techie racket stuff
- easy to forget **Programming is Fun**
  + daresay ... empowering, rewarding
  - (no this is distracting) ideas come to life, some form or another (flower, frankenstein // fractal, inf loop)
- past few years, very grateful for these summer events
  - 2019 fish, show entries
  - 2020 qs, show ... something
- this year, I was happy to co-organize with Stephen
- quick prompt --- get to the macros already!
- ...
- best of all, went toward community resource
- still accepting entries, challenge you to try out a macro idea and have fun
- just like the real thing, except I'm out of prizes
- that's right every participant got a few stickers including an exclusive
  limited edition macro pillow sticker
- ...
- picture of ben + pillow
- old pictures of lab
- wrapping up,
  - links
  - and big thank you to stephen, the overseer, and the participantsn
  - optimistic thank you to the organizer(s) of next years event
- "Thank you." (end recording)



